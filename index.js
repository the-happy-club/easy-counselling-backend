"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();
exports.onConversationCreated = functions.firestore.document("Conversations/{conversationID}").onCreate((snapshot, context) => {
    let data = snapshot.data();
    let conversationID = context.params.conversationID;
    if (data) {
        let members = data.members;
        for (let index = 0; index < members.length; index++) {
            let currentUserID = members[index];
            let remainingUserIDs = members.filter((u) => u !== currentUserID);
            // eslint-disable-next-line no-loop-func
            remainingUserIDs.forEach((m) => {
                return admin.firestore().collection("Users").doc(m).get().then((_doc) => {
                    let userData = _doc.data();
                    if (userData) {
                        return admin.firestore().collection("Users").doc(currentUserID).collection("Conversations").doc(m).create({
                            "conversationID": conversationID,
                            "image": userData.image,
                            "name": userData.name,
                            "unseenCount": 0,
                        });
                    }
                    return null;
                }).catch(() => { return null; });
            });
        }
    }
    return null;
});
exports.onConversationUpdated = functions.firestore.document("Conversations/{chatID}").onUpdate((change, context) => {
    let data = change === null || change === void 0 ? void 0 : change.after.data();
    if (data) {
        let members = data.members;
        let lastMessage = data.messages[data.messages.length - 1];
        for (let index = 0; index < members.length; index++) {
            let currentUserID = members[index];
            let remainingUserIDs = members.filter((u) => u !== currentUserID);
            // eslint-disable-next-line no-loop-func
            remainingUserIDs.forEach((u) => {
                return admin.firestore().collection("Users").doc(currentUserID).collection("Conversations").doc(u).update({
                    "lastMessage": lastMessage.message,
                    "timestamp": lastMessage.timestamp,
                    "type": lastMessage.type,
                    "unseenCount": admin.firestore.FieldValue.increment(1),
                });
            });
        }
    }
    return null;
});


// MAKE SURE THAT COUNSELLOR IDS ARE ALSO IN USER IDS eg. CQnueR6kIRWmrXxVS7UhU2Elo5c2

exports.changeCounsellorCase = functions.firestore.document("Users/{userID}").onUpdate((change, context) => {
    let userID = context.params.userID;
    let mainCounsellor = "";
    let newData = change === null || change === void 0 ? void 0 : change.after.data();
    let oldData = change.before.data();
    if (oldData.wantsCounsellor === false && newData.wantsCounsellor === true) {
        let minCases = 10000;
        // eslint-disable-next-line promise/always-return
        var documents = admin.firestore().collection("Counsellors").get().then(snapshot => {
                snapshot.forEach(doc => {
                    let cases = doc.data().cases;
                    if (cases < minCases) {
                        minCases = cases;
                        mainCounsellor = doc.id;
                    }
                });
                // eslint-disable-next-line promise/no-nesting
                return admin.firestore().collection("Counsellors").doc(mainCounsellor).update({
                    "users": admin.firestore.FieldValue.arrayUnion(userID),
                    "cases": admin.firestore.FieldValue.increment(1)
                }).then((value) => {
                    let userConversationRef = admin.firestore().collection("Users").doc(userID).collection("Conversations");
                    let conversationsRef = admin.firestore().collection("Conversations").doc();
                    conversationsRef.create({
                        "members": [userID, mainCounsellor],
                        "ownerID": userID,
                        "messages": []
                    });
                    return conversationsRef;
                });
            })
            .catch(err => {
                console.log('Error getting documents', err);
            });
    }
    if (oldData.wantsCounsellor === true && newData.wantsCounsellor === false) {
        var user = admin.firestore().collection("Users").doc(userID);
        let counsellorID;
        //console.log(user.id); //WJpazz
        return user.collection("Conversations").get().then(snapshot => {
            // eslint-disable-next-line promise/no-nesting
            let a = snapshot.forEach(doc => {
                console.log("deleting");
                counsellorID = doc.id;
                let conversationID = doc.data().conversationID;
                console.log("conversation id");
                console.log(conversationID);
                let c = admin.firestore().collection("Conversations").doc(conversationID);
                c.delete();
                //let b = admin.firestore().collection("Conversations").doc(conversationID).ref.delete();
                doc.ref.delete();
                let counsellor = admin.firestore().collection("Counsellors").doc(counsellorID);
                let a = counsellor.update({
                    "users": admin.firestore.FieldValue.arrayRemove(user.id),
                    "cases": admin.firestore.FieldValue.increment(-1)
                });
                return a;
            });
            // eslint-disable-next-line promise/no-nesting
            return admin.firestore().collection("Users").doc(counsellorID).collection("Conversations").get().then(snapshot => {
                return snapshot.forEach(doc => {
                    if (doc.id === userID) {
                        return doc.ref.delete();
                    }
                    return null;
                });
            });

        });
    }
    return null;
});